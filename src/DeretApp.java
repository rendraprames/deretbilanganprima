import java.util.*;

public class DeretApp {
    public static void main(String[] args) {
        Scanner inputAngka = new Scanner(System.in);
        System.out.println("+------------------------------------------------+");
        System.out.println("|          Program deret bilangan prima !        |");
        System.out.println("+------------------------------------------------+");
        System.out.print(" From  : ");
        int angkaAwal = inputAngka.nextInt();
        System.out.print(" To    : ");
        int angkaAkhir = inputAngka.nextInt();
        System.out.println("+------------------------------------------------+");
        System.out.println(" Bilangan prima dari " + angkaAwal + " sampai " + angkaAkhir +" :");
        for(int i=angkaAwal;i<=angkaAkhir;i++){
            int tampung = 0;
            for(int j=1;j<=i;j++){
                if (i%j==0){
                    tampung+=1;
                }
            }
            if (tampung==2){
                System.out.print(" "+ i + " ");
            }
        }
        System.out.println(" ");
        System.out.println("+------------------------------------------------+");
        inputAngka.close();
    }
}
